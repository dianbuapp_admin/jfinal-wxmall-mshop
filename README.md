# jfinal-wxmall-mshop

#### 项目介绍
wxmall 多店铺版本

基于Jfinal Jboot微服务开发的多店铺商城系统

项目包含微信公众号，小程序，pc，app多个客户端，app采用hbuilder开发

管理平台包含商户管理后台，平台运维后台

接口工程包含app接口，小程序接口

项目演示地址：

- 1. pc端: http://pc.maiankang.cn
- 1. android app下载地址：http://pc.maiankang.cn/maiankang.apk (使用手机浏览器打开直接下载)
- 1. 公众号端：

商户后台截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162307_f070391b_471938.png "TIM图片20180823161945.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162320_62f498fa_471938.png "TIM图片20180823162019.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162330_f4378345_471938.png "TIM图片20180823162059.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162416_4b04b820_471938.png "TIM图片20180823162121.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162425_00c4db3f_471938.png "TIM图片20180823162155.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162438_91a59b4f_471938.png "TIM图片20180823162241.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162449_319fc61a_471938.png "TIM图片20180823162223.png")

运维后台截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162855_ca8aa728_471938.png "TIM图片20180823162658.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162903_ecabce68_471938.png "TIM图片20180823162731.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162911_ad0a3566_471938.png "TIM图片20180823162756.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/162920_8b1287e3_471938.png "TIM图片20180823162838.png")

#### 软件架构



- JDK8 + 
- 基于JFinal核心框架开发
- 基于JBoot微服务开发

代码结构说明
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/164420_daa0fb9a_471938.png "TIM图片20180823164410.png")

1. wxmall-model工程
    提供数据模型，由系统生成，不可更改其代码，在model模块中找到ModelGenApp类，运行其main方法，生成model实体
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/164714_b00dc475_471938.png "TIM图片20180823164706.png")
2. wxmall-service-api工程
    接口抽象工程
    基础接口由系统生成，每个数据表对应一个service基础类，提供model的增删改查基本方法
    在service工程中找到ServiceCodeGenApp类，运行其main方法，生成基础接口类，生成后，可以修改，自定义其他业务逻辑接口方法
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/165306_905be8b6_471938.png "TIM图片20180823165142.png")
3. wxmall-service-provider工程
    接口实现工程，微服务，服务提供者
    同样可通过provider工程的gen目录下的ServiceImplCodeGenApp类来生成基础接口实现类，运行其main方法即可
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/170250_decfc3c7_471938.png "TIM图片20180823170242.png")
4. wxmall-sched
    wxmall分布式调度框架，最简洁的调度实现代码
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/170428_8229c133_471938.png "TIM图片20180823170420.png")
5. wxmall-web-core
    所有web工程依赖的核心工程，提供web基础类封装
6. wxmall-web-admin
    商户管理后台工程
7. wxmall-web-plat
    平台运维管理后台工程
8. wxmall-web-pc
    pc商城客户端工程
9. wxmall-web-mobile
    微信公众号客户端工程
10. wxmall-web-api
    小程序接口工程
11. wxmall-app-api
    app接口工程
12. wxmall-wxsdk工程
    微信接口api封装工程
13. wxmall-app-h5
    wxmall hbuilder 开发的app工程
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/171207_3673db2d_471938.png "TIM图片20180823171138.png")
14. wxmall-weapp
    wxmall小程序客户端工程
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/171444_098630ce_471938.png "TIM图片20180823171435.png")

#### 安装部署

1. wxmall是maven管理项目，获取源码后，导入到eclipse中
2. 导入数据库
3. 首先运行provider工程
    provider工程直接找到工程下面的StartApp类，运行其main方法启动应用，使用内置服务器undertow
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/172159_0ddc4f6d_471938.png "TIM图片20180823172151.png")
4. 然后再运行web工程
5. web工程推荐使用jetty-run插件运行，只支持jetty9以上的服务器
    eclipse中安装jetty插件
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/171931_b1065c22_471938.png "TIM图片20180823171924.png")
6. 每个web工程都做如下操作
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/172339_5a182fce_471938.png "TIM图片20180823172331.png")

7. 打包部署
    使用maven 命令     clean package appassembler:generate-daemons
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/172452_00cf049e_471938.png "TIM图片20180823172437.png")

#### 配置说明

1. wxmall-service-api工程中的公共配置说明如下
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/172701_fd3e803e_471938.png "TIM图片20180823172654.png")
2. 数据库配置如下
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/172836_cdb0be0c_471938.png "TIM图片20180823172829.png")
3. jboot相关分布式部署配置请参考jboot文档

#### 使用说明

1. 本项目是收费项目
2. 付费后获取最新最全源码
3. 代码最终解析权属于点步科技

合作请加作者微信

![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/140151_888c0a4f_471938.jpeg "微信图片_20190215135628.jpg")
